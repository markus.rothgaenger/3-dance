every monthly goal includes the corresponding chapters in the thesis

# 1. Month
- research & experiments
  - spatial information decoding
  - (summarization)
    - paper by nazzia?!
    - experiment by averaging over activations + decoding
      -> this might work as the captions do not deviate too much from each other.. 
    - simple statistical analysis!?
  - 
- introduction/motivation & related work

# 2. Month
- 3D scene graph **generation**
  - Khademi and Schulte should be a good start
- method & implementation


# 3. Month
- **using** the 3D scene graph
  - either VQA or 3D labeling
  - for VQA, Khademi and Schulte again
- method & implementation

# 4. Month
preferably end of implementation
- results & evaluation

# 5. Month
- results & evaluation

# 6. Month
- conclusion & further work
- finalization