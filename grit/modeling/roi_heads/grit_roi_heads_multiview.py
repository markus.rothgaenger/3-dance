import logging
import math
from typing import Dict, List, Tuple

import numpy as np
import torch

from detectron2.modeling.roi_heads.fast_rcnn import fast_rcnn_inference
from detectron2.modeling.roi_heads.roi_heads import ROI_HEADS_REGISTRY
from detectron2.structures import Boxes, Instances

from grit.data.custom_dataset_mapper import ObjDescription
from .grit_roi_heads import GRiTROIHeadsAndTextDecoder

logger = logging.getLogger(__name__)


@ROI_HEADS_REGISTRY.register()
class GRiTROIHeadsAndTextDecoderMultiview(GRiTROIHeadsAndTextDecoder):
    def _forward_box_multiview(self, features, proposals, decode_text=False):
        if self.mult_proposal_score:
            if len(proposals) > 0 and proposals[0].has('scores'):
                proposal_scores = [p.get('scores') for p in proposals]
            else:
                proposal_scores = [p.get('objectness_logits') for p in proposals]

        features = [features[f] for f in self.box_in_features]
        head_outputs = []
        prev_pred_boxes = None
        image_sizes = [x.image_size for x in proposals]

        for k in range(self.num_cascade_stages):
            if k > 0:
                proposals = self._create_proposals_from_boxes(
                    prev_pred_boxes, image_sizes,
                    logits=[p.objectness_logits for p in proposals])
            predictions = self._run_stage(features, proposals, k)
            prev_pred_boxes = self.box_predictor[k].predict_boxes(
                (predictions[0], predictions[1]), proposals)
            head_outputs.append((self.box_predictor[k], predictions, proposals))

        # TODO: scores not working for multi view..
        scores_per_stage = [h[0].predict_probs(h[1], h[2]) for h in head_outputs]

        image_dict = {}
        for stage in scores_per_stage:
            for i, image in enumerate(stage):
                if image_dict.get(i) is None:
                    image_dict[i] = torch.zeros_like(image)
                image_dict[i] += image



        print(image_dict)
        print(image_dict[0].shape)
        # three stages
        #   three images (stack)
        #       stage results
        # scores = [
        #     sum(list(scores_per_image)) * (1.0 / self.num_cascade_stages)
        #     for scores_per_image in zip(*scores_per_stage)
        # ]

        # moved up..
        predictor, predictions, proposals = head_outputs[-1]
        boxes = predictor.predict_boxes(
            (predictions[0], predictions[1]), proposals)
        print(boxes[0].shape)
        scores = [tuple(sum(list(scores_per_image)) for scores_per_image in zip(*score_p)) for score_p in scores_per_stage]
        if self.mult_proposal_score:
            scores = [(s * ps[:, None]) ** 0.5 for s, ps in zip(scores, proposal_scores)]

        # ..was here

        # fallback to detectron fast_rcnn_inference.
        pred_instances, _ = fast_rcnn_inference(
            boxes,
            scores,
            image_sizes,
            predictor.test_score_thresh,
            predictor.test_nms_thresh,
            predictor.test_topk_per_image
        )

        for i, pred_instance in enumerate(pred_instances):
            if len(pred_instance.pred_boxes) > 0:
                # I might be able to do this batched as well..
                object_features = self.object_feat_pooler([f[i].unsqueeze(0) for f in features], [pred_instance.pred_boxes])
                object_features = object_features.view(
                    object_features.shape[0], object_features.shape[1], -1).permute(0, 2, 1).contiguous()

                pred_instance.object_features = object_features
                if decode_text:
                    text_decoder_output = self.text_decoder({'object_features': object_features})
                    if self.beam_size > 1 and self.test_task == "ObjectDet":
                        pred_boxes = []
                        pred_scores = []
                        pred_classes = []
                        pred_object_descriptions = []

                        for beam_id in range(self.beam_size):
                            pred_boxes.append(pred_instance.pred_boxes.tensor)
                            # object score = sqrt(objectness score x description score)
                            pred_scores.append((pred_instance.scores *
                                                torch.exp(text_decoder_output['logprobs'])[:, beam_id]) ** 0.5)
                            pred_classes.append(pred_instance.pred_classes)
                            for prediction in text_decoder_output['predictions'][:, beam_id, :]:
                                # convert text tokens to words
                                description = self.tokenizer.decode(prediction.tolist()[1:], skip_special_tokens=True)
                                pred_object_descriptions.append(description)

                        merged_instances = Instances(image_sizes[0])
                        if torch.cat(pred_scores, dim=0).shape[0] <= predictor.test_topk_per_image:
                            merged_instances.scores = torch.cat(pred_scores, dim=0)
                            merged_instances.pred_boxes = Boxes(torch.cat(pred_boxes, dim=0))
                            merged_instances.pred_classes = torch.cat(pred_classes, dim=0)
                            merged_instances.pred_object_descriptions = ObjDescription(pred_object_descriptions)
                        else:
                            pred_scores, top_idx = torch.topk(
                                torch.cat(pred_scores, dim=0), predictor.test_topk_per_image)
                            merged_instances.scores = pred_scores
                            merged_instances.pred_boxes = Boxes(torch.cat(pred_boxes, dim=0)[top_idx, :])
                            merged_instances.pred_classes = torch.cat(pred_classes, dim=0)[top_idx]
                            merged_instances.pred_object_descriptions = \
                                ObjDescription(ObjDescription(pred_object_descriptions)[top_idx].data)

                        pred_instances[i] = merged_instances
                    else:
                        # object score = sqrt(objectness score x description score)
                        pred_instance.scores = (pred_instance.scores *
                                                torch.exp(text_decoder_output['logprobs'])) ** 0.5

                        pred_object_descriptions = []
                        for prediction in text_decoder_output['predictions']:
                            # convert text tokens to words
                            description = self.tokenizer.decode(prediction.tolist()[1:], skip_special_tokens=True)
                            pred_object_descriptions.append(description)
                        pred_instance.pred_object_descriptions = ObjDescription(pred_object_descriptions)
                else:
                    pred_instance.pred_object_descriptions = ObjDescription(np.repeat('', len(pred_instance)))

        return pred_instances

    def forward_multiview(self, features, proposals, decode_text=False):
        pred_instances = self._forward_box_multiview(
            features,
            proposals,
            decode_text=decode_text
        )
        pred_instances = self.forward_with_given_boxes(features, pred_instances)
        return pred_instances, {}

    def forward_with_given_boxes(
        self, features: Dict[str, torch.Tensor], instances: List[Instances]
    ) -> List[Instances]:
        """
        Use the given boxes in `instances` to produce other (non-box) per-ROI outputs.

        This is useful for downstream tasks where a box is known, but need to obtain
        other attributes (outputs of other heads).
        Test-time augmentation also uses this.

        Args:
            features: same as in `forward()`
            instances (list[Instances]): instances to predict other outputs. Expect the keys
                "pred_boxes" and "pred_classes" to exist.

        Returns:
            list[Instances]:
                the same `Instances` objects, with extra
                fields such as `pred_masks` or `pred_keypoints`.
        """
        # assert not self.training
        assert instances[0].has("pred_boxes") and instances[0].has("pred_classes")

        instances = self._forward_mask(features, instances)
        instances = self._forward_keypoint(features, instances)
        return instances

    def _forward_mask(self, features: Dict[str, torch.Tensor], instances: List[Instances]):
        """
        Forward logic of the mask prediction branch.

        Args:
            features (dict[str, Tensor]): mapping from feature map names to tensor.
                Same as in :meth:`ROIHeads.forward`.
            instances (list[Instances]): the per-image instances to train/predict masks.
                In training, they can be the proposals.
                In inference, they can be the boxes predicted by R-CNN box head.

        Returns:
            In training, a dict of losses.
            In inference, update `instances` with new fields "pred_masks" and return it.
        """
        if not self.mask_on:
            return instances

        if self.mask_pooler is not None:
            features = [features[f] for f in self.mask_in_features]
            boxes = [x.pred_boxes for x in instances]
            features = self.mask_pooler(features, boxes)
        else:
            features = {f: features[f] for f in self.mask_in_features}
        return self.mask_head(features, instances)

    def _forward_keypoint(self, features: Dict[str, torch.Tensor], instances: List[Instances]):
        """
        Forward logic of the keypoint prediction branch.

        Args:
            features (dict[str, Tensor]): mapping from feature map names to tensor.
                Same as in :meth:`ROIHeads.forward`.
            instances (list[Instances]): the per-image instances to train/predict keypoints.
                In training, they can be the proposals.
                In inference, they can be the boxes predicted by R-CNN box head.

        Returns:
            In training, a dict of losses.
            In inference, update `instances` with new fields "pred_keypoints" and return it.
        """
        if not self.keypoint_on:
            return instances

        if self.keypoint_pooler is not None:
            features = [features[f] for f in self.keypoint_in_features]
            boxes = [x.pred_boxes for x in instances]
            features = self.keypoint_pooler(features, boxes)
        else:
            features = {f: features[f] for f in self.keypoint_in_features}
        return self.keypoint_head(features, instances)