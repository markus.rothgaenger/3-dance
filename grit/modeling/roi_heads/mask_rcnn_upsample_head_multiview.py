from typing import List

from detectron2.modeling import ROI_MASK_HEAD_REGISTRY
from detectron2.modeling.roi_heads import MaskRCNNConvUpsampleHead
from detectron2.modeling.roi_heads.mask_head import mask_rcnn_inference
from detectron2.structures import Instances


@ROI_MASK_HEAD_REGISTRY.register()
class MaskRCNNConvUpsampleHeadMultiview(MaskRCNNConvUpsampleHead):
    def forward(self, x, instances: List[Instances]):
        """
        Args:
            x: input region feature(s) provided by :class:`ROIHeads`.
            instances (list[Instances]): contains the boxes & labels corresponding
                to the input features.
                Exact format is up to its caller to decide.
                Typically, this is the foreground instances in training, with
                "proposal_boxes" field and other gt annotations.
                In inference, it contains boxes that are already predicted.

        Returns:
            A dict of losses in training. The predicted "instances" in inference.
        """
        x = self.layers(x)

        mask_rcnn_inference(x, instances)
        return instances
