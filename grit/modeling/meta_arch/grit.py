from typing import Dict, List, Optional, Tuple

import torch
from detectron2.config import configurable
from detectron2.modeling.meta_arch.build import META_ARCH_REGISTRY
from detectron2.modeling.meta_arch.rcnn import GeneralizedRCNN
from detectron2.structures import Instances


@META_ARCH_REGISTRY.register()
class GRiT(GeneralizedRCNN):
    @configurable
    def __init__(
            self,
            **kwargs):
        super().__init__(**kwargs)
        assert self.proposal_generator is not None

    @classmethod
    def from_config(cls, cfg):
        ret = super().from_config(cfg)
        return ret

    def inference(
            self,
            batched_inputs: Tuple[Dict[str, torch.Tensor]],
            detected_instances: Optional[List[Instances]] = None,
            do_postprocess: bool = True,
    ):
        assert not self.training
        assert detected_instances is None

        images = self.preprocess_image(batched_inputs)
        features = self.backbone(images.tensor)
        proposals, _ = self.proposal_generator(images, features, None)
        results, _ = self.roi_heads(features, proposals)
        if do_postprocess:
            assert not torch.jit.is_scripting(), \
                "Scripting is not supported for postprocess."
            return GRiT._postprocess(
                results, batched_inputs, images.image_sizes), features
        else:
            return results

    def forward_multiview(self, batched_inputs: List[Dict[str, torch.Tensor]], decode_text=False) -> Tuple[
        List[Dict[str, Instances]], torch.Tensor]:
        """
        Assume one batch to include multiple views of the same object.
        Args:
            batched_inputs:

        Returns:
            TODO: what is the structure of proposals return value?
        """
        images = self.preprocess_image(batched_inputs)
        features: torch.Tensor = self.backbone(images.tensor)

        prop_features = [features[f] for f in self.proposal_generator.in_features]
        clss_per_level, reg_pred_per_level, agn_hm_pred_per_level = \
            self.proposal_generator.centernet_head(prop_features)
        grids = self.proposal_generator.compute_grids(prop_features)

        proposals, _ = self.proposal_generator.inference(
            images, clss_per_level, reg_pred_per_level,
            agn_hm_pred_per_level, grids)

        results, _ = self.roi_heads.forward_multiview(features, proposals, decode_text=decode_text)

        # TODO: do I need postprocessing?
        return GRiT._postprocess(results, batched_inputs, images.image_sizes), features

    def forward(self, batched_inputs: List[Dict[str, torch.Tensor]]):
        if not self.training:
            return self.inference(batched_inputs)

        images = self.preprocess_image(batched_inputs)

        gt_instances = [x["instances"].to(self.device) for x in batched_inputs]

        targets_task = batched_inputs[0]['task']
        for anno_per_image in batched_inputs:
            assert targets_task == anno_per_image['task']

        features = self.backbone(images.tensor)
        proposals, proposal_losses = self.proposal_generator(
            images, features, gt_instances)
        proposals, roihead_textdecoder_losses = self.roi_heads(
            features, proposals, gt_instances, targets_task=targets_task)

        losses = {}
        losses.update(roihead_textdecoder_losses)
        losses.update(proposal_losses)

        return losses
