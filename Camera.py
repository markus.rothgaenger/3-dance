import numpy as np


class Camera:
    step = 0
    _steps: int
    R: float
    Y: float
    offset = 0

    @property
    def steps(self):
        return self._steps

    @steps.setter
    def steps(self, value):
        if self.offset > 0:
            self.offset *= value/self.steps
        self._steps = value
        self.pi_frac = np.pi / np.ceil(self.steps / 2)

    @property
    def end(self):
        return not self.step < self.steps

    def next_step(self):
        if self.step < self.steps:
            self.step += 1

    @property
    def y(self):
        return self.Y

    @y.setter
    def y(self, value):
        self.Y = value

    @property
    def x(self):
        return self.R * np.cos((self.step + self.offset) * self.pi_frac)

    @property
    def z(self):
        return self.R * np.sin((self.step + self.offset) * self.pi_frac)

    def __init__(self, steps: int = 32, R = 4, Y = 2.5) -> None:
        self.steps = steps
        self.R = R
        self.Y = Y

    def reset(self):
        self.step = 0
