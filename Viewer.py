from typing import List

import numpy as np
import open3d as o3d
import open3d.visualization.gui as gui
import open3d.visualization.rendering as rendering


# This example displays a point cloud and if you Ctrl-click on a point
# (Cmd-click on macOS) it will show the coordinates of the point.
# This example illustrates:
# - custom mouse handling on SceneWidget
# - getting a the depth value of a point (OpenGL depth)
# - converting from a window point + OpenGL depth to world coordinate
class Viewer:
    def __init__(self):
        # We will create a SceneWidget that fills the entire window, and then
        # a label in the lower left on top of the SceneWidget to display the
        # coordinate.
        app = gui.Application.instance
        self.window = app.create_window("3D DenseCap Visualizer", 1280, 720)
        # Since we want the label on top of the scene, we cannot use a layout,
        # so we need to manually layout the window's children.
        self.window.set_on_layout(self._on_layout)
        self.widget3d = gui.SceneWidget()
        self.window.add_child(self.widget3d)
        self.info = gui.Label("")
        self.info.visible = False
        self.window.add_child(self.info)
        self.widget3d.scene = rendering.Open3DScene(self.window.renderer)
        self.widget3d.scene.set_background([0, 0, 0, 1])

        self.bboxes: List[o3d.geometry.OrientedBoundingBox] = []
        self.bbox_materials: List[rendering.MaterialRecord] = []
        self.bbox_identifiers: List[str] = []

        self.default_color = (0.7, 0.7, 0.7, 0.7)
        self.highlight_color = (0.9, 0.4, 0.1, 1)

        self.label = None

        self.widget3d.set_on_mouse(self._on_mouse_widget3d)

    def setup_camera(self):
        bounds = self.widget3d.scene.bounding_box
        center = bounds.get_center()
        self.widget3d.setup_camera(60, bounds, center)
        self.widget3d.look_at(center, center - [0, 0, 3], [0, -1, 0])

    def add_geometry(self, identifier: str, geometry: o3d.geometry.Geometry, mat: rendering.MaterialRecord):
        self.widget3d.scene.add_geometry(identifier, geometry, mat)

    def add_bbox(self, identifier: str, bbox: o3d.geometry.OrientedBoundingBox, mat: rendering.MaterialRecord):
        mat.base_color = self.default_color

        self.bboxes.append(bbox)
        self.bbox_materials.append(mat)
        self.bbox_identifiers.append(identifier)
        self.add_geometry(identifier, bbox, mat)

    def _on_layout(self, layout_context):
        r = self.window.content_rect
        self.widget3d.frame = r
        pref = self.info.calc_preferred_size(layout_context,
                                             gui.Widget.Constraints())
        self.info.frame = gui.Rect(r.x,
                                   r.get_bottom() - pref.height, pref.width,
                                   pref.height)

    def _on_mouse_widget3d(self, event):
        # We could override BUTTON_DOWN without a modifier, but that would
        # interfere with manipulating the scene.
        if event.type == gui.MouseEvent.Type.BUTTON_DOWN and event.is_modifier_down(
                gui.KeyModifier.CTRL):

            def depth_callback(depth_image):
                # Coordinates are expressed in absolute coordinates of the
                # window, but to dereference the image correctly we need them
                # relative to the origin of the widget. Note that even if the
                # scene widget is the only thing in the window, if a menubar
                # exists it also takes up space in the window (except on macOS).
                x = event.x - self.widget3d.frame.x
                y = event.y - self.widget3d.frame.y
                # Note that np.asarray() reverses the axes.
                depth = np.asarray(depth_image)[y, x]

                min_i = -1

                if depth == 1.0:  # clicked on nothing (i.e. the far plane)
                    text = ""
                else:
                    world = self.widget3d.scene.camera.unproject(
                        x, y, depth, self.widget3d.frame.width,
                        self.widget3d.frame.height)

                    min_dist = np.inf
                    for i, box in enumerate(self.bboxes):
                        dist = np.linalg.norm(box.get_center() - world)
                        if dist < min_dist:
                            min_i = i
                            min_dist = dist

                    text = f"{self.bbox_identifiers[min_i]}"

                    # text = "({:.3f}, {:.3f}, {:.3f})".format(
                    #     world[0], world[1], world[2])

                def update_materials():
                    for i in range(len(self.bboxes)):
                        if i == min_i:
                            self.bbox_materials[i].base_color = self.highlight_color
                        else:
                            self.bbox_materials[i].base_color = self.default_color

                        self.widget3d.scene.modify_geometry_material(self.bbox_identifiers[i], self.bbox_materials[i])

                # This is not called on the main thread, so we need to
                # post to the main thread to safely access UI items.
                def update_label():
                    self.info.text = text
                    self.info.visible = (text != "")

                    if self.label is not None:
                        self.widget3d.remove_3d_label(self.label)

                    if min_i > -1:
                        self.label: gui.Label3D = self.widget3d.add_3d_label(self.bboxes[min_i].get_center(), text)
                        self.label.color = gui.Color(0.8, 0.8, 0.8, 1.0)
                    # We are sizing the info label to be exactly the right size,
                    # so since the text likely changed width, we need to
                    # re-layout to set the new frame.
                    self.window.set_needs_layout()

                gui.Application.instance.post_to_main_thread(
                    self.window, update_label)
                gui.Application.instance.post_to_main_thread(
                    self.window, update_materials
                )

            self.widget3d.scene.scene.render_to_depth_image(depth_callback)
            return gui.Widget.EventCallbackResult.HANDLED
        return gui.Widget.EventCallbackResult.IGNORED
