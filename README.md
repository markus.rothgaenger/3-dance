## 3D Dense Captioning PROTOTYPE

This implementation is a prototype and **NOT** complete!

This is based off of https://github.com/JialianW/GRiT.
Details regarding the instructions specified in the above mentioned repository.
There, you also find links to the pretrained GRiT model weights that are required for running this application.

### installation
we use version python 3.8 and  version 2.0.1 of pytorch
```
conda create -n grit python=3.8 -y
conda activate grit
pip install torch torchaudio torchvision
```

install detectron2. This requires that you have **CUDA 11.7** installed, matching the cuda version pytorch was compiled with!
```
git clone https://github.com/facebookresearch/detectron2.git
cd detectron2
git checkout cc87e7ec
pip install -e .
```

if the above is done, continue to install the requirements from this repo:
```
pip install -r requirements.txt
```

## perform 3D dense captioning
1. Get the pretrained GRiT model [weights](https://datarelease.blob.core.windows.net/grit/models/grit_b_densecap.pth) and put them in to `./models`
2. start this application using: `python main.py --test-task DenseCap --config-file config/gritty.yaml --opts MODEL.WEIGHTS models/grit_b_densecap.pth`. Once ready, `awaiting connection` is printed to the shell.
3. start the accompanying [Unity project](https://gitlab.ub.uni-bielefeld.de/markus.rothgaenger/densecapunity) by loading the SampleScene and pressing Play.

Once the model has been processed through the unity engine, the RGB and depth data is saved in a file called `step_data.pickle` in the root folder of this project. This can be used to skip the data generation path by adding the `--skip` command line flag:
You can also download a sample data file [here](https://uni-bielefeld.sciebo.de/s/n1lIS515QVCw2aY) and put it in the root folder.

To use the `step_data.pickle` file, run the application using:
`python main.py --skip --test-task DenseCap --config-file config/gritty.yaml --opts MODEL.WEIGHTS models/grit_b_densecap.pth`

## interaction
Once the viewer application is started, `left mouse` + `drag` can be used to rotate the camera around the object. The captions for bounding boxes can be accessed by holding `Ctrl` and clicking on the 3D bounding box. This then highlights the bounding box and displays the corresponding caption in the lower left corner.




