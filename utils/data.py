import numpy as np
from numpy import typing as npt

from UnityTCPServer import PCDataMap

rng = np.random.default_rng()


# https://stackoverflow.com/a/16562028
# https://www.itl.nist.gov/div898/handbook/eda/section3/eda35h.htm#Z-Scores
def reject_outliers(np_data: npt.NDArray, m=3.5):
    if np_data.shape[0] < 2:
        return np_data

    """this only works for multidim data."""
    d = np.abs(np_data - np.median(np_data, axis=0))
    mdev = np.median(d, axis=0)
    # TODO: what does it mean if one element int mdev is zero?!
    s = d / mdev if mdev.all() else np.zeros_like(d)
    return np_data[np.all(s < m, axis=1)]


def get_box_samples(pc_data: PCDataMap, box: npt.NDArray, filter_outliers=False, n_samples=200, variance_factor=0.5) -> npt.NDArray:
    """pc_data is the point cloud data with size (height, width, <x,y,z>)"""
    xmin, ymin, xmax, ymax = box

    samples = []
    x_width = xmax - xmin
    y_width = ymax - ymin

    mean = np.array([xmin + x_width / 2, ymin + y_width / 2])
    var = np.array([x_width / 5, y_width / 5]) * variance_factor

    max_tries = n_samples * 4
    tries = 0

    # for x, y in ps:
    while len(samples) < n_samples - 1 and tries < max_tries:
        tries += 1
        x, y = (
            rng.normal(mean, var, (1, 2))
            .clip(np.array([xmin, ymin]), np.array([xmax, ymax]))
            .round()
            .astype(np.int32)
        )[0]

        try:
            p = pc_data[y][x]
            samples.append(p)
        except KeyError:
            continue

    samples = np.array(samples)

    if filter_outliers:
        return reject_outliers(samples)

    return samples
