from typing import Dict

import torch


def camera_struct_to_tensor(camera_position: Dict[str, float]) -> torch.Tensor:
    return torch.tensor([camera_position['x'], camera_position['y'], camera_position['z']])