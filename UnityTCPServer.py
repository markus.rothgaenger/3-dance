import json
import multiprocessing
import socket
import time
import numpy as np
import numpy.typing as npt
from multiprocessing import Queue
from typing import Dict, Any

HOST = "127.0.0.1"
PORT = 32791

PCDataMap = Dict[int, Dict[int, npt.NDArray]]


class PointCloudData:
    points: PCDataMap
    finished: bool
    rgbPath: str

    @staticmethod
    def from_json(obj_dict: Dict[str, Any]):
        data = PointCloudData()
        points = obj_dict["points"]

        if obj_dict["height"] is None:
            raise Exception("cannot deserialize point data for height == None")

        height = obj_dict["height"]

        data.points = {}

        for p in points:
            i = (height - 1) - p["i"]
            j = p['j']
            pos = np.array(p["worldPos"])

            if data.points.get(i) is None:
                data.points[i] = {j: pos}
                continue

            data.points[i][j] = pos

        if obj_dict.get("finished") is not None:
            data.finished = obj_dict["finished"]
        if obj_dict.get("rgbPath") is not None:
            data.rgbPath = obj_dict["rgbPath"]

        return data


def merge_pc_dicts(a: PCDataMap, b: PCDataMap) -> PCDataMap:
    merged = a.copy()
    a_keys = a.keys()

    for i, js in b.items():
        if i not in a_keys:
            merged[i] = js
            continue

        for j, pt in js:
            if j in merged[i].keys():
                raise Exception("unexpected double reference in merge")
            merged[i][j] = pt

    return merged


class UnityTCPServer:
    cam_pos_queue: Queue
    next_model_queue: Queue
    point_cloud_queue: Queue

    def __init__(self):
        self.cam_pos_queue = Queue()
        self.next_model_queue = Queue()
        self.point_cloud_queue = Queue()
        self.server_worker = None

    def client_loop(self, conn: socket.socket, addr):
        sender_worker = multiprocessing.Process(
            target=self.start_sender, args=(conn, )
        )
        sender_worker.start()

        json_builder = ""

        decode_count = 0

        while True:
            data = conn.recv(1000000)  # 1 MB
            if not data:
                print(f"connection interrupted by {addr}")
                sender_worker.kill()
                break

            data_str = data.decode()

            split = data_str.split("<START>")
            for substr in split:
                if substr == "":
                    continue

                json_builder += substr

                if "<END>" in substr:
                    json_builder = json_builder.replace("<END>", "")
                    print("decoding")
                    decoded_data: PointCloudData = PointCloudData.from_json(
                        json.loads(json_builder)
                    )
                    decode_count += 1
                    self.point_cloud_queue.put(decoded_data)
                    json_builder = ""

        sender_worker.join()

    def server_loop(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.bind((HOST, PORT))
            s.listen(10)

            try:
                while True:
                    print("awaiting connection")
                    conn, addr = s.accept()

                    with conn:
                        print(f"client {addr} connected")
                        self.client_loop(conn, addr)

            except KeyboardInterrupt:
                s.close()

    def start_listener(self, point_cloud_queue: Queue):
        while True:
            if point_cloud_queue.empty():
                time.sleep(1)
                continue

            data = point_cloud_queue.get()
            print("received data from socket!")
            print(data)

    def start_sender(self, conn: socket.socket):
        while True:
            if self.cam_pos_queue.empty() and self.cam_pos_queue.empty():
                time.sleep(0.1)
                continue

            if not self.cam_pos_queue.empty():
                print("sending cam data")
                data_object = {
                    "type": 0, # CAM POS ENUM
                    "data": json.dumps(self.cam_pos_queue.get())
                }
                conn.send(bytes(f"{json.dumps(data_object)}<END>", "utf-8"))
            if not self.next_model_queue.empty():
                print("sending next model data")
                data_object = {
                    "type": 1,  # NEXT MODEL ENUM
                    "data": json.dumps(self.next_model_queue.get())
                }
                conn.send(bytes(f"{json.dumps(data_object)}<END>", "utf-8"))


    def start(self):
        self.server_worker = multiprocessing.Process(target=self.server_loop)
        self.server_worker.start()

    def stop(self):
        if self.server_worker is not None:
            self.server_worker.terminate()
