import argparse
import gc
import multiprocessing as mp
import os
import pickle
import sys
from pathlib import Path

import torchvision.transforms as tf
from torch.utils.data import DataLoader, RandomSampler, Subset
from torchvision.datasets import ImageFolder
from tqdm import tqdm
# from xgboost import XGBClassifier

from grit.data.custom_build_augmentation import build_custom_augmentation
from grit.data.custom_dataset_dataloader import build_custom_train_loader
from grit.data.custom_dataset_mapper import CustomDatasetMapper
from launch_deepspeed import launch_deepspeed
from utils.three_dance_data import CarClassImageDataset

sys.path.insert(0, 'third_party/CenterNet2/projects/CenterNet2/')
from third_party.CenterNet2.projects.CenterNet2.centernet.config import add_centernet_config
import time
from datetime import timedelta
from typing import List

from detectron2.data.transforms import Augmentation
from detectron2.engine import PeriodicCheckpointer, default_setup
from detectron2.solver import build_lr_scheduler, build_optimizer
from detectron2.utils import comm
from detectron2.utils.events import CommonMetricPrinter, EventStorage, JSONWriter, TensorboardXWriter
from fvcore.common.timer import Timer
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC

from grit.custom_solver import build_custom_optimizer
from utils.data import get_box_samples, reject_outliers
from utils.other import camera_struct_to_tensor

import cv2
import numpy as np
import open3d as o3d
import open3d.visualization.gui as gui
import torch
import torch.nn.functional as f
from detectron2.checkpoint import DetectionCheckpointer
from detectron2.modeling import build_model
import detectron2.data.transforms as T

from UnityTCPServer import PointCloudData, UnityTCPServer, merge_pc_dicts
from Viewer import Viewer

from grit.modeling.meta_arch.grit import GRiT
from detectron2.config import get_cfg
from detectron2.utils.logger import setup_logger
from logging import Logger

from Camera import Camera
from grit.config import add_grit_config

MAPPING = {0: 'BACK_LEFT', 1: 'BACK_RIGHT', 2: 'FRONT_LEFT', 3: 'FRONT_RIGHT'}
CLASS_MAPPING = {v: k for k, v in MAPPING.items()}
IMG_PATH = "/home/markus/dev/grit_socket/images/"
N_SAMPLES = 200
SIMILARITY_THRESHOLD = 0.05

# CAR
# MIN_BOX_DIST = 0.4  # minimum distance of new bbox in unity units to already existing bboxes
# MAX_MATCH_BOX_DIST = 0.25  # maximum distance of matches to current bbox
MIN_BOX_DIST = 0.15  # minimum distance of new bbox in unity units to already existing bboxes
MAX_MATCH_BOX_DIST = 0.1  # maximum distance of matches to current bbox


def setup_cfg(args):
    cfg = get_cfg()
    # if args.cpu:
    #     cfg.MODEL.DEVICE = "cpu"
    add_centernet_config(cfg)
    add_grit_config(cfg)
    cfg.merge_from_file(args.config_file)
    if 'MODEL.DEVICE' in args.opts:
        cfg.MODEL.DEVICE = -1

    cfg.merge_from_list(args.opts)
    # Set score_threshold for builtin models
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = args.confidence_threshold
    cfg.MODEL.PANOPTIC_FPN.COMBINE.INSTANCES_CONFIDENCE_THRESH = (
        args.confidence_threshold
    )
    if args.test_task:
        cfg.MODEL.TEST_TASK = args.test_task
    cfg.MODEL.BEAM_SIZE = 1
    cfg.MODEL.ROI_HEADS.SOFT_NMS_ENABLED = False
    cfg.USE_ACT_CHECKPOINT = False
    cfg.freeze()
    return cfg


def get_parser():
    parser = argparse.ArgumentParser(description="3D DenseCap Demo")
    parser.add_argument(
        "--config-file",
        default="",
        metavar="FILE",
        help="path to config file",
    )
    parser.add_argument(
        "--confidence-threshold",
        type=float,
        default=0.5,
        help="Minimum score for instance predictions to be shown",
    )
    parser.add_argument(
        "--test-task",
        type=str,
        default="",
        help="Choose a task to have GRiT perform",
    )
    parser.add_argument(
        "--opts",
        help="Modify config options using the command-line 'KEY VALUE' pairs",
        default=[],
        nargs=argparse.REMAINDER,
    )
    parser.add_argument(
        "--skip",
        help="load step data from file instead of generating via unity",
        action='store_true'
    )
    parser.add_argument(
        "--learn-spatial-predictor",
        help="fit the spatial predictor to the current embeddings",
        action='store_true'
    )
    parser.add_argument(
        "--view-persistence",
        help="include the previous activation of the roi when decoding",
        action="store_true"
    )
    parser.add_argument(
        "--train-persistence",
        help="start training for multi view persistence optimization.",
        action="store_true"
    )
    return parser


def convert_index_to_window_index(window_lens, index):
    current_start_idx = 0
    for batch, max_i in enumerate(window_lens):
        if index > current_start_idx + max_i - 1:
            current_start_idx += max_i
            continue

        return batch, index - current_start_idx

    raise Exception("invalid conversion args")


def get_model(args):
    cfg = setup_cfg(args)
    # TODO MR: create GRIT model
    model: GRiT = build_model(cfg)
    model.eval()

    checkpointer = DetectionCheckpointer(model)
    checkpointer.load(cfg.MODEL.WEIGHTS)

    input_format = cfg.INPUT.FORMAT
    assert input_format in ["RGB", "BGR"], input_format

    aug = T.ResizeShortestEdge(
        [cfg.INPUT.MIN_SIZE_TEST, cfg.INPUT.MIN_SIZE_TEST], cfg.INPUT.MAX_SIZE_TEST
    )

    return model, input_format, aug, cfg


def do_test(model: GRiT, aug, storage: EventStorage):
    model.eval()
    model.roi_heads.finetune = False
    shapenet_img_root = Path("data/car_images")
    transform = tf.Compose([
        tf.Resize((200, 200)),
        tf.PILToTensor(),
    ])
    shapenet_dataset = ImageFolder(shapenet_img_root, transform=transform)
    sampler = RandomSampler(data_source=shapenet_dataset)
    shapenet_data_loader = DataLoader(shapenet_dataset, sampler=sampler, batch_size=8)

    shapenet_object_features = []
    shapenet_labels = []

    with torch.no_grad():
        for batch, (images, labels) in enumerate(tqdm(shapenet_data_loader)):
            for i, image in enumerate(images):
                image = image.numpy().transpose(1, 2, 0)
                height, width = image.shape[:2]
                image = aug.get_transform(image).apply_image(image)
                image = torch.as_tensor(image.astype("float32").transpose(2, 0, 1))
                inputs = [{"image": image, "height": height, "width": width}]

                predictions, stuff = model.forward(inputs)

                instances = predictions[0]["instances"]
                if len(instances.pred_boxes) < 1:
                    print("skipping empty instances")
                    continue

                n_instances, _, _ = instances.object_features.shape
                shapenet_object_features.extend(instances.object_features.cpu())
                shapenet_labels.extend(np.repeat(labels[i].item(), n_instances))

            if len(shapenet_object_features) > 500:
                break

    shapenet_features = torch.stack(shapenet_object_features)
    shapenet_features = shapenet_features.flatten(start_dim=1)

    # x_train, x_test, y_train, y_test = train_test_split(
    #     shapenet_features, shapenet_labels, test_size=0.2, random_state=42
    # )

    # classifier = XGBClassifier(predictor="gpu_predictor", gpu_id=model.device.index)
    # classifier.fit(x_train, y_train)

    # accuracy = classifier.score(x_test, y_test)
    # storage.put_scalars(test_accuracy=accuracy)

    # print(f"test accuracy: {accuracy:.3f}")

    # prev_model = None
    # for i, batch_start in enumerate(range(0, len(x_train), BATCH_SIZE)):
    #     model = XGBClassifier(predictor="gpu_predictor")
    #     model.fit(x_train[batch_start:batch_start + BATCH_SIZE], y_train[batch_start:batch_start + BATCH_SIZE],
    #               xgb_model=prev_model)
    #     prev_model = model
    #     print(f"finished fit for batch {i + 1}..")
    #     print(f"test accuracy: {model.score(x_test, y_test):.3f}")


def do_train(cfg, model: GRiT, logger: Logger, resume=False):
    """taken from grit repo"""
    model.eval()  # this might be dangerous?!
    # this disables text decoding and roi feature extracting..
    model.roi_heads.finetune = True

    aug = T.ResizeShortestEdge(
        [cfg.INPUT.MIN_SIZE_TEST, cfg.INPUT.MIN_SIZE_TEST], cfg.INPUT.MAX_SIZE_TEST
    )

    input_format = cfg.INPUT.FORMAT
    assert input_format in ["RGB", "BGR"], input_format

    if cfg.SOLVER.USE_CUSTOM_SOLVER:
        optimizer = build_custom_optimizer(cfg, model)
    else:
        assert cfg.SOLVER.OPTIMIZER == 'SGD'
        assert cfg.SOLVER.CLIP_GRADIENTS.CLIP_TYPE != 'full_model'
        optimizer = build_optimizer(cfg, model)
    scheduler = build_lr_scheduler(cfg, optimizer)

    checkpointer = DetectionCheckpointer(
        model, cfg.OUTPUT_DIR, optimizer=optimizer, scheduler=scheduler
    )

    start_iter = checkpointer.resume_or_load(
        cfg.MODEL.WEIGHTS, resume=resume).get("iteration", -1) + 1
    if not resume:
        start_iter = 0
    max_iter = cfg.SOLVER.MAX_ITER if cfg.SOLVER.TRAIN_ITER < 0 else cfg.SOLVER.TRAIN_ITER

    periodic_checkpointer = PeriodicCheckpointer(
        checkpointer, cfg.SOLVER.CHECKPOINT_PERIOD, max_iter=max_iter
    )

    writers = (
        [
            CommonMetricPrinter(max_iter),
            JSONWriter(os.path.join(cfg.OUTPUT_DIR, "metrics.json")),
            TensorboardXWriter(cfg.OUTPUT_DIR),
        ]
        if comm.is_main_process()
        else []
    )

    mapper = CustomDatasetMapper(cfg, True, augmentations=build_custom_augmentation(cfg, True))
    vg_data_loader = build_custom_train_loader(cfg, mapper=mapper)
    car_dataset = CarClassImageDataset(img_dir='data/car_images_model_sort')
    data_loader = DataLoader(car_dataset, batch_size=1)

    rng = np.random.default_rng()

    logger.info("Starting training from iteration {}".format(start_iter))
    with EventStorage(start_iter) as storage:
        step_timer = Timer()
        data_timer = Timer()
        start_time = time.perf_counter()

        for (images, classes, cam_poses), vg_data, iteration in zip(data_loader, vg_data_loader, range(start_iter, max_iter)):
            data_time = data_timer.seconds()
            storage.put_scalars(data_time=data_time)
            step_timer.reset()
            # iteration = iteration + 1
            storage.step()
            inputs = []

            # VG STEP
            model.train()
            model.roi_heads.finetune = False
            loss_dict = model(vg_data)
            model.eval()
            model.roi_heads.finetune = True

            vg_losses = sum(
                loss for k, loss in loss_dict.items())
            assert torch.isfinite(vg_losses).all(), loss_dict

            # MULTIVIEW STEP
            with torch.no_grad():
                for image in images:
                    image = image.squeeze().numpy().transpose(1, 2, 0)
                    if input_format == "RGB":
                        # whether the model expects BGR inputs or RGB
                        image = image[:, :, ::-1]
                    height, width = image.shape[:2]
                    image = aug.get_transform(image).apply_image(image)
                    image = torch.as_tensor(image.astype("float32").transpose(2, 0, 1))
                    inputs.append({"image": image, "height": height, "width": width})

            classes = np.array([CLASS_MAPPING[cls[0]] for cls in classes]).astype(int)

            features = []

            for img in inputs:
                predictions, img_features = model.forward([img])
                features.append(img_features["p7"])

            features = torch.stack(features)

            contrastive_loss = torch.zeros(()).to(model.device)
            multiview_loss = torch.zeros(()).to(model.device)

            for i in range(len(features)):
                i_features: torch.Tensor = f.normalize(torch.flatten(features[i]), dim=0)
                i_cam_pos = cam_poses[i]

                i_class = classes[i]
                other_class_mask = classes != i_class
                same_class_mask = ~other_class_mask

                if np.sum(same_class_mask) < 2:
                    print("not enough same class samples, skipping..")
                    continue

                other_class_features = features[other_class_mask]
                same_class_features = features[same_class_mask]

                # TODO: preferably sample from all three other classes..
                try:
                    # this follows towers of babel
                    # TODO do not simply take first same class feature
                    same_feature_vec = f.normalize(torch.flatten(same_class_features[0]), dim=0)
                    same_class_sim = torch.exp(torch.dot(i_features, same_feature_vec))

                    other_samples_idx = rng.choice(np.arange(len(other_class_features)), size=3, replace=False)
                    other_samples = other_class_features[other_samples_idx]

                    other_class_sim_sum = 0
                    for vec in other_samples:
                        other_class_sim_sum += torch.exp(torch.dot(
                            i_features,
                            f.normalize(torch.flatten(vec), dim=0)
                        ))

                    contrastive_loss += -torch.log(same_class_sim/(same_class_sim + other_class_sim_sum))
                except ValueError as e:
                    print(e)
                    continue

                for j in range(i+1, len(features)):
                    j_features: torch.Tensor = f.normalize(torch.flatten(features[j]), dim=0)
                    j_cam_pos = cam_poses[j]

                    cam_distance = torch.linalg.vector_norm(i_cam_pos - j_cam_pos)
                    dot = torch.dot(i_features, j_features)

                    # if cam_distance < 2:
                    # dot product distance is most similar the greater it is..
                    multiview_loss += torch.abs(dot / cam_distance)

            losses = contrastive_loss + multiview_loss
            if not torch.is_nonzero(losses):
                optimizer.zero_grad(set_to_none=True)
                periodic_checkpointer.step(iteration)
                continue

            assert torch.isfinite(losses).all(), losses

            if comm.is_main_process():
                storage.put_scalars(
                    contrastive_loss=contrastive_loss,
                    multiview_loss=multiview_loss,
                    total_loss=losses)

            optimizer.zero_grad(set_to_none=True)
            vg_losses.backward()
            losses.backward()
            optimizer.step()
            # model.zero_grad(set_to_none=True)

            storage.put_scalar(
                "lr", optimizer.param_groups[0]["lr"], smoothing_hint=False)

            step_time = step_timer.seconds()
            storage.put_scalars(time=step_time)
            data_timer.reset()
            scheduler.step()

            if (cfg.TEST.EVAL_PERIOD > 0
                    and iteration % cfg.TEST.EVAL_PERIOD == 0
                    and iteration != max_iter):
                do_test(model, aug, storage)
                model.roi_heads.finetune = True
                comm.synchronize()

            if iteration % 20 == 0 or iteration == max_iter:
                for writer in writers:
                    writer.write()
            periodic_checkpointer.step(iteration)

        total_time = time.perf_counter() - start_time
        logger.info(
            "Total training time: {}".format(
                str(timedelta(seconds=int(total_time)))))


def camera_sweep(unity_connector: UnityTCPServer):
    camera = Camera(steps=16, R=3, Y=1.75)

    camera_positions = []
    images = []
    point_clouds = []

    # first, let's find the most confident (box average) view
    while True:
        if camera.end:
            print("last position reached")
            break

        camera_position = {
            "x": camera.x,
            "y": camera.y,
            "z": camera.z,
        }

        unity_connector.cam_pos_queue.put(camera_position)

        data: PointCloudData = unity_connector.point_cloud_queue.get()
        pts = data.points

        while not data.finished:
            data: PointCloudData = unity_connector.point_cloud_queue.get()
            pts = merge_pc_dicts(data.points, pts)

        rgb_img = None
        while rgb_img is None:
            rgb_img = cv2.imread(data.rgbPath)

            if rgb_img is None:
                time.sleep(0.05)

        camera_positions.append(camera_position)
        images.append(rgb_img)
        point_clouds.append(pts)

        camera.next_step()

    return camera_positions, images, point_clouds


def get_data_from_unity(model: GRiT, input_format: str, aug: Augmentation, view_persistence=True):
    camera = Camera(steps=8, R=3, Y=1.75)
    unity_server = UnityTCPServer()
    unity_server.start()

    step_data = {}
    best_view_score = np.inf * -1
    best_view_index = -1

    # first, let's find the most confident (box average) view    
    if camera.end:
        print("last position reached")
        return

    unity_server.cam_pos_queue.put(
        {
            "x": camera.x,
            "y": camera.y,
            "z": camera.z,
        }
    )

    data: PointCloudData = unity_server.point_cloud_queue.get()
    pts = data.points

    while not data.finished:
        data: PointCloudData = unity_server.point_cloud_queue.get()
        pts = merge_pc_dicts(data.points, pts)

    rgb_img = None
    while rgb_img is None:
        rgb_img = cv2.imread(data.rgbPath)
        # plt.imshow(rgb_img)

        if rgb_img is None:
            time.sleep(0.1)

    with torch.no_grad():  # https://github.com/sphinx-doc/sphinx/issues/4258
        if input_format == "RGB":
            # whether the model expects BGR inputs or RGB
            rgb_img = rgb_img[:, :, ::-1]
        height, width = rgb_img.shape[:2]
        # plt.imshow(rgb_img)
        image = aug.get_transform(rgb_img).apply_image(rgb_img)
        # plt.imshow(image)
        image = torch.as_tensor(image.astype("float32").transpose(2, 0, 1))
        inputs = [{"image": image, "height": height, "width": width}]

        predictions = model(inputs)
        scores = predictions[0]["instances"].to(torch.device("cpu")).scores
        avg_score = scores.mean()

        if avg_score > best_view_score:
            best_view_score = avg_score
            best_view_index = camera.step

    camera.next_step()

    best_view_index = 7
    print(f"set best view with index {best_view_index}")
    camera.offset = best_view_index
    camera.steps = 32
    camera.reset()

    previous_pred_instances = None
    previous_boxes_samples = None

    axx = 0
    camera.y = 0.75
    while True:
        if camera.y > 5:
            break
        if camera.end:
            print("last position reached")
            break

        unity_server.cam_pos_queue.put(
            {
                "x": camera.x,
                "y": camera.y,
                "z": camera.z,
            }
        )

        # while point_cloud_queue.empty():
        #     time.sleep(0.2)
        #     # print("waiting..")

        data: PointCloudData = unity_server.point_cloud_queue.get()
        pts = data.points

        # if unity_server.point_cloud_queue.empty():
        #     time.sleep(1)
        #     continue

        # TODO: introduce some kind of base structure for messages
        while not data.finished:
            data: PointCloudData = unity_server.point_cloud_queue.get()
            pts = merge_pc_dicts(data.points, pts)

        rgb_img = None
        while rgb_img is None:
            rgb_img = cv2.imread(data.rgbPath)

            if rgb_img is None:
                time.sleep(0.1)

        # rgb_img = cv2.resize(rgb_img, (320, 180), interpolation=cv2.INTER_CUBIC)

        # TODO MR: move this to own predictor class..
        with torch.no_grad():  # https://github.com/sphinx-doc/sphinx/issues/4258
            if input_format == "RGB":
                # whether the model expects BGR inputs or RGB
                rgb_img = rgb_img[:, :, ::-1]
            original_height, original_width = rgb_img.shape[:2]
            # TODO: i might need to adapt the height and width given to the roi heads
            # as soon as the transformation changes the resolution..
            image = aug.get_transform(rgb_img).apply_image(rgb_img)
            image = torch.as_tensor(image.astype("float32").transpose(2, 0, 1))
            inputs = [{"image": image, "height": original_height, "width": original_width}]

            images = model.preprocess_image(inputs)
            features = model.backbone(images.tensor)
            proposals, _ = model.proposal_generator(images, features, None)
            # next line is the forward call to GRIT_ROI_HEAD -> text decoder
            if view_persistence:
                results, _ = model.roi_heads(
                    features,
                    proposals,
                    previous_pred_instances=previous_pred_instances,
                    previous_boxes_samples=previous_boxes_samples,
                    point_cloud_data=pts,
                    original_height=original_height,
                    original_width=original_width
                )
            else:
                results, _ = model.roi_heads(
                    features,
                    proposals
                )

            previous_pred_instances = results[0]
            predictions = GRiT._postprocess(results, inputs, images.image_sizes)

        instances = predictions[0]["instances"].to(torch.device("cpu"))
        boxes = instances.pred_boxes
        scores = instances.scores
        descriptions = instances.pred_object_descriptions
        object_features = instances.object_features

        # free gpu memory
        del predictions
        del instances

        boxes_samples = []
        for box in boxes.tensor:
            box_samples = get_box_samples(pts, box, filter_outliers=True, n_samples=100, variance_factor=0.7)
            boxes_samples.append(box_samples)

        previous_boxes_samples = boxes_samples

        # step_data[camera.step] = {
        step_data[axx] = {
            "pc": pts,
            "rgb": rgb_img,
            "boxes": boxes,
            "boxes_samples": boxes_samples,
            "scores": scores,
            "descriptions": descriptions,
            "object_features": object_features,
        }

        # pcd = o3d.geometry.PointCloud()
        # pcd.points = o3d.utility.Vector3dVector(pts)
        # pcs.append(pcd)

        # camera.next_step()
        axx += 1
        camera.y += 0.5
    with open('step_data.pickle', 'wb') as handle:
        pickle.dump(step_data, handle, protocol=pickle.HIGHEST_PROTOCOL)

    unity_server.stop()
    return step_data


def main(args):
    # GRIT STUFF
    # mp.set_start_method("spawn", force=True)

    # logger = setup_logger(name="unity hook")

    model, input_format, aug, cfg = get_model(args)

    default_setup(cfg, args)
    logger = setup_logger(output=cfg.OUTPUT_DIR, distributed_rank=comm.get_rank(), color=False, name="gritty")
    logger.info("Arguments: " + str(args))

    if args.train_persistence:
        do_train(cfg, model, logger)

    if args.skip:
        with open('step_data.pickle', 'rb') as handle:
            step_data = pickle.load(handle)
    else:
        step_data = get_data_from_unity(model, input_format, aug, args.view_persistence)
    
    # spatial encoding classifier stuff
    # 0 right back
    # 1 right front
    # 2 left front
    # 3 left back
    
    for x in step_data.values():
        labels = []
    
        for i, samples in enumerate(x['boxes_samples']):
            if not len(samples):
                labels.append(-1)
                continue
            center_of_mass = samples.mean(axis=0)
            # z zero axis is divider..
            # class 0: z < 0; 1: z >= 0
            front = center_of_mass[2] < 0
            right = center_of_mass[0] < 0
            labels.append(
                0 if right and not front
                else 1 if right and front
                else 2 if not right and front
                else 3
            )
    
        x['labels'] = np.array(labels, dtype=np.int8)
    
    labels = [label for x in step_data.values() for label in x['labels']]
    # object feature SIM STUFF
    # texts = [description for x in step_data.values() for description in x['descriptions'].data]
    scores = np.array([score for x in step_data.values() for score in x['scores'].data])
    object_features = [x['object_features'] for x in step_data.values()]
    sort_idx = np.argsort(scores)[::-1]
    lens = [len(x['descriptions'].data) for x in step_data.values()]
    
    object_features = torch.vstack(object_features)
    
    # TODO: try this classifier for other models
    # TODO: try influence of umap followed by SVM
    # try to classify the embeddings..
    # classification_features = torch.flatten(object_features, start_dim=1)
    # clf: Pipeline = None
    # if args.learn_spatial_predictor:
    #     linear_svm = SVC(kernel='linear', C=0.025)
    
    #     x_train, x_test, y_train, y_test = train_test_split(
    #         classification_features, labels, test_size=0.2, random_state=42
    #     )
    
    #     clf = make_pipeline(StandardScaler(), linear_svm)
    #     clf.fit(x_train, y_train)
    #     score = clf.score(x_test, y_test)
    #     print(f"spatial prediction score: {score:.3F}")
    #     with open('spatial_predictor.pickle', 'wb') as handle:
    #         pickle.dump(clf, handle, protocol=pickle.HIGHEST_PROTOCOL)
    # else:
    #     with open('spatial_predictor.pickle', 'rb') as handle:
    #         clf = pickle.load(handle)
    
    # y = clf.predict(classification_features)
    
    # calculate embedding cosine similarity
    max_embeds, _ = object_features.max(dim=1)
    # normalize the max token embeddings
    max_normalized = f.normalize(max_embeds, p=2, dim=1)
    # calculate the cosine similarity
    max_dist = max_normalized.matmul(max_normalized.T)
    max_dist = max_dist.new_ones(max_dist.shape) - max_dist
    max_dist = max_dist.numpy()
    
    print(f"distance range: {max_dist.min()} to {max_dist.max()}")
    print(f"mean: {max_dist.mean()}, median: {np.median(max_dist.flatten())}")
    
    count = 0
    box_point_clouds: List[o3d.geometry.PointCloud] = []
    
    img_out_dir = "output"
    if not os.path.exists(img_out_dir):
        os.makedirs(img_out_dir)
    
    app = gui.Application.instance
    app.initialize()
    viewer = Viewer()
    
    cleaned_sort_idx = []
    for i in sort_idx:
        i_batch, i_idx = convert_index_to_window_index(lens, i)
        i_image_ref = step_data[i_batch]
        i_samples = i_image_ref['boxes_samples'][i_idx]
        if i_samples.shape[0] < 2:  # TODO: SET CONST VALUE
            print(f"filter empty samples {i_samples.shape}")
            print(i_image_ref["descriptions"].data[i_idx])
            continue
    
        cleaned_sort_idx.append(i)
    
    # TODO: clustering in object feature space might also be feasible?!
    for i in cleaned_sort_idx:
        i_batch, i_idx = convert_index_to_window_index(lens, i)
        i_image_ref = step_data[i_batch]
        i_text: str = i_image_ref['descriptions'].data[i_idx]
        i_samples = i_image_ref['boxes_samples'][i_idx]
        if i_samples.shape[0] < 2:  # TODO: SET CONST VALUE
            print("ignore box with (nearly) empty samples")
            continue
    
        # TODO: set denied list of keywords..
        if "background" in i_text or "white" in i_text or "sky" in i_text:
            continue
    
        i_pc = o3d.geometry.PointCloud()
        pcd_np = reject_outliers(i_samples)
        # flip z and y to convert to right handed points
        pcd_np = pcd_np[:, [0, 2, 1]]
        i_pc.points = o3d.utility.Vector3dVector(pcd_np)
    
        # compute point cloud distance..
        near_other_box = False
        for _pc in box_point_clouds:
            distance = np.abs(np.array(_pc.compute_point_cloud_distance(i_pc)).mean())
    
            if distance < MIN_BOX_DIST:
                near_other_box = True
                break
    
        if near_other_box:
            # print("reject text bc of low distance to other box")
            # TODO: check text similarity to other match!
            # is that necessary? or might it be useful to merge them?
            continue
    
        accepted_matches = 0
        matched_object_features = [i_image_ref['object_features'][i_idx]]
        matched_texts = []
    
        # TODO: resort the search. search for spatially close matches..
        for j in cleaned_sort_idx:
            if i == j or max_dist[i][j] > SIMILARITY_THRESHOLD:
                continue
    
            j_batch, j_idx = convert_index_to_window_index(lens, j)
    
            j_image_ref = step_data[j_batch]
            j_samples = j_image_ref['boxes_samples'][j_idx]
    
            if j_samples.shape[0] < 10:  # TODO: SET CONST VALUE
                print("ignore match with (nearly) empty samples")
                continue
    
            j_text = j_image_ref['descriptions'].data[j_idx]
            # print(f"\t{j}: \t{j_text}")
            j_pc = o3d.geometry.PointCloud()
            j_pc.points = o3d.utility.Vector3dVector(
                reject_outliers(j_samples)[:, [0, 2, 1]]
            )
    
            pc_distance = np.abs(np.array(i_pc.compute_point_cloud_distance(j_pc)).mean())
            print(f"{i_text} -> {j_text} -> {pc_distance}")
    
            if pc_distance > MAX_MATCH_BOX_DIST:
                # print(f"reject match due to high distance: {com_distance}")
                continue
    
            i_samples = np.append(i_samples, j_samples, axis=0)
            matched_object_features.append(j_image_ref['object_features'][j_idx])
            matched_texts.append(j_text)
            accepted_matches += 1
    
        if len(i_samples) < 1:
            # print("empty samples!")
            continue
        if accepted_matches < 2:
            # print(f"reject text due to too few matches ({accepted_matches})!")
            continue

        matched_object_features = torch.stack(matched_object_features)
    
        # TODO do I want to remove samples from rejected classes?
        # class_features = torch.flatten(matched_object_features, start_dim=1)
        # predicted_y = clf.predict(class_features)
        # print(predicted_y)
        # counts = np.bincount(predicted_y)
        # max_class = np.argmax(counts)
        # print(f"max class: {max_class}")
    
        # if max_class == 0:
        #     if "front" in i_text or "left" in i_text:
        #         continue
        # elif max_class == 1:
        #     if "back" in i_text or "left" in i_text:
        #         continue
        # elif max_class == 2:
        #     if "back" in i_text or "right" in i_text:
        #         continue
        # else:
        #     if "front" in i_text or "right" in i_text:
        #         continue        
        # mask = predicted_y == max_class
    
        # # filter by generated text
        # # 0 right back
        # # 1 right front
        # # 2 left front
        # # 3 left back
        # for text_i, text in enumerate(matched_texts):
        #     if max_class == 0:
        #         if "front" in text or "left" in text:
        #             mask[text_i + 1] = False
        #     elif max_class == 1:
        #         if "back" in text or "left" in text:
        #             mask[text_i + 1] = False
        #     elif max_class == 2:
        #         if "back" in text or "right" in text:
        #             mask[text_i + 1] = False
        #     else:
        #         if "front" in text or "right" in text:
        #             mask[text_i + 1] = False
    
        # matched_texts = np.array(matched_texts)[mask[1:]]
        # mask out object features classified as spatially wrong
        # matched_object_features = matched_object_features[mask]
    
        with torch.no_grad():
            merged_object_features = matched_object_features.mean(axis=0)
            prediction = \
                model.roi_heads.text_decoder(
                    {'object_features': merged_object_features.unsqueeze(0).cuda()}
                )['predictions'][0]
            # TODO: reject if spatially wrong!
            description_after = model.roi_heads.tokenizer.decode(prediction.tolist()[1:], skip_special_tokens=True)
            prediction = \
                model.roi_heads.text_decoder(
                    {'object_features': i_image_ref['object_features'][i_idx].unsqueeze(0).cuda()}
                )['predictions'][0]
            description_before = model.roi_heads.tokenizer.decode(prediction.tolist()[1:], skip_special_tokens=True)
    
            print(
                f"{accepted_matches} matches: {description_before} ({', '.join(matched_texts)}) -> {description_after}")
    
        # set classificatoin color, green -> front, red -> back
        # color = np.array([1, 0, 0]) if y[i] == 0 else np.array([0, 1, 0]) if y[i] == 1 else np.array([0, 0, 1]) if y[
        #                                                                                                                i] == 2 else np.array(
        #     [1, 1, 1])
        color = np.random.rand(3, 1)
        # print(f"text: {i_text}")
        i_pc = i_pc.paint_uniform_color(color)
        try:
            bbox: o3d.geometry.OrientedBoundingBox = i_pc.get_oriented_bounding_box()
        except RuntimeError as e:
            print(e)
            print("unable to align bounding box, probably 2D data..")
            continue
        bbox.color = color
    
        mat = o3d.visualization.rendering.MaterialRecord()
        mat.shader = "defaultUnlit"
        mat.point_size = 5 * viewer.window.scaling
        mat.base_color = (color[0].item(), color[1].item(), color[2].item(), 1)
    
        line_mat = o3d.visualization.rendering.MaterialRecord()
        line_mat.shader = "unlitLine"
        line_mat.line_width = 4 * viewer.window.scaling
        line_mat.base_color = (color[0].item(), color[1].item(), color[2].item(), 1)
    
        viewer.add_geometry(f"{description_after}_{count}_pc", i_pc, mat)
        viewer.add_bbox(f"{description_after}_{count}_bbox", bbox, line_mat)
    
        count += 1
    
        box_point_clouds.append(i_pc)
    
    viewer.setup_camera()
    app.run()


if __name__ == "__main__":
    args = get_parser()
    args.add_argument("--output-dir-name", type=str, default='./output/gritty-deepspeed')
    args.add_argument("--num-gpus-per-machine", type=int, default=1)
    args.add_argument("--num_machines", type=int, default=1)
    args.add_argument(
        "--machine-rank", type=int, default=0, help="the rank of this machine (unique per machine)"
    )
    args = args.parse_args()
    args.dist_url = 'tcp://127.0.0.1:{}'.format(12345)
    print("Command Line Args:", args)
    launch_deepspeed(
        main,
        args.num_gpus_per_machine,
        num_machines=args.num_machines,
        machine_rank=args.machine_rank,
        dist_url=args.dist_url,
        args=(args,),
    )
